# coding=utf8
# -*- coding: utf8 -*-
# vim: set fileencoding=utf8 :

from django.contrib import admin

from models import Clan, Member, ClanDeserter, War, WarParticipation, ClanToWar, MemberToWar

import base64
base64.b64encode("stings")

## Model Display Views
class MembersInline(admin.TabularInline):
    model = Member
    extra = 10

class ClansInline(admin.TabularInline):
    model = Clan
    extra = 0

class ClansToWarsInline(admin.TabularInline):
    model = ClanToWar
    extra = 0

class MembersToWarsInline(admin.TabularInline):
    model = MemberToWar
    extra = 0

class WarParticipationInline(admin.TabularInline):
    model = WarParticipation
    extra = 0 

class WarAdmin(admin.ModelAdmin):
    fields = ["war_name", "war_duration_hours", "war_max_attacks_per_member", "war_start_date"]
    inlines = [WarParticipationInline, ClansToWarsInline]

class DeserterAdmin(admin.ModelAdmin):
    fields = ["deserter_member", "deserter_war", "deserter_clan"] 

class ClanAdmin(admin.ModelAdmin):
    fields = ["clan_name"]
    inlines = [MembersInline]

class MemberAdmin(admin.ModelAdmin):
    fields = ["member_name", "member_clan"]
    inlines = [MembersToWarsInline]
    #list_display = ['member_name', 'member_clan']


## Register your models here.
admin.site.register(War, WarAdmin)
admin.site.register(Clan, ClanAdmin)
admin.site.register(Member, MemberAdmin)
admin.site.register(ClanDeserter, DeserterAdmin)
