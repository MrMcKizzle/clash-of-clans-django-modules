# coding=utf8
# -*- coding: utf8 -*-
# vim: set fileencoding=utf8 :

from django.db import models

import base64
base64.b64encode("stings")

# Create your models here.
class War(models.Model):
    """
    Represents a war.    
    """
    # Primary Fields
    war_name = models.CharField(max_length=255)
    war_duration_hours = models.DecimalField(max_digits=32, decimal_places=16)
    war_start_date = models.DateField(auto_now_add=False)
    war_max_attacks_per_member = models.IntegerField()
 
    def __str__(self):
        return self.war_name

class MemberToWar(models.Model):
    """
    Represents a member 
    """
    mtw_clan = models.ForeignKey("ClanToWar", default=0)
    mtw_member = models.ForeignKey("Member", default=0)
    mtw_member_rank = models.IntegerField()
    mtw_member_participated = models.BooleanField(default=False)

class ClanToWar(models.Model):
    """
    Associates a clan with a war. 
    """
    ctw_clan = models.ForeignKey("Clan")
    ctw_war  = models.ForeignKey("War")

    def __str__(self):
        return "%s + %s" % (self.ctw_clan.clan_name, self.ctw_war.war_name)

class Clan(models.Model):
    """
    All of the clans.  
    """
    # Primary Fields
    clan_name = models.CharField("Clan Name", max_length=255)

    def __str__(self):
        return self.clan_name

class Member(models.Model):
    """
    Members to clan associations. 
    """
    # Primary Fields
    member_name = models.CharField(max_length=255)

    # Foreign Keys
    member_clan = models.ForeignKey('Clan', related_name='member_clan')

    def __str__(self):
        return self.member_name

class WarParticipation(models.Model):
    """
    Simple member particpation statistics for each war. 
    """
    # Primary Fields
    wp_stars_won = models.IntegerField(default=0)
    wp_gold_looted   = models.IntegerField(default=0)
    wp_elixir_looted = models.IntegerField(default=0)
    wp_dark_elixir_looted = models.IntegerField(default=0)

    # Foreign Keys
    wp_attacker_clan = models.ForeignKey('Clan', related_name='wp_attacker_clan', default=0)
    wp_defender_clan = models.ForeignKey('Clan', related_name='wp_defender_clan', null=True, blank=True)
    wp_attacker = models.ForeignKey('Member', related_name='wp_attacker', default=0)
    wp_defender = models.ForeignKey('Member', related_name='wp_defender', null=True, blank=True)
    wp_war      = models.ForeignKey('War', related_name='wp_war', default=0)

    def __str__(self):
        return "%s vs %s" % (self.wp_attacker, self.wp_defender)

class ClanDeserter(models.Model):
    """
    Who has deserted the clan?  
    """
    # Primary Fields

    # Foreign Keys
    deserter_member = models.ForeignKey('Member', null=False, default=0) 
    deserter_war    = models.ForeignKey('War',    null=False, default=0)
    deserter_clan   = models.ForeignKey('Clan',   null=False, default=0)

    def __str__(self):
        return "%s -- %s" % (self.deserter_member.member_name, self.deserter_clan.clan_name)
    

