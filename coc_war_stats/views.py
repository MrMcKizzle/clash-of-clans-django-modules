from django.http import HttpResponse
from django.shortcuts import render
from django.views import generic
from django.db.models import Q as AdvancedQuery
from models import Clan, Member
import pprint

# Simple list that displays all of the clans. 
# TODO: Modify this in the future so that this page is searchable and 
#   navigatable. 
#def clan_list(request):
#    clan_list = Clan.objects.order_by('-clan_name')[:]
#    clans = ', '.join([clan.clan_name for clan in clan_list])
#    return HttpResponse(clans)

class ClanIndex(generic.ListView):
    """
    List of all of the clans. 
    """
    template_name = 'coc_war_stats/clan_list.html'
    context_object_name = 'the_clans'

    def get_queryset(self):
        return Clan.objects.order_by('-id')[:].reverse()


class ClanInfo(generic.DetailView):
    """
    Display quick statistics about the clan, it's members and who
    has been participating. 
    """
    model=Clan 
    template_name = 'coc_war_stats/clan_details.html'

    def get_context_data(self, *args, **kwargs):
        ctx = super(generic.DetailView, self).get_context_data(
                *args, **kwargs
                )
        ctx['members'] = Member.objects.filter(member_clan=ctx['object'].id)
        ctx['failed_to_participate'] = "derp!"

        return ctx

#class ClanWarsIndex(generic.ListView):
#    template_name = 'coc_war_stats/clan_wars.html'
#    context_object_name = 'clan_wars'
#    clan_a_id = 0
#
#    def dispatch(self, *args, **kargs):
#        if kargs['pk'] is not None:
#            self.clan_a_id = kargs['pk']
#
#        pp = pprint.PrettyPrinter(indent=4) 
#        print("------------------------------- ARGS -------------------------")
#        pp.pprint(args)
#        print("------------------------------- KARGS ------------------------")
#        pp.pprint(kargs)
#        return super(generic.ListView, self).dispatch(*args, **kargs)
#
#    def get_queryset(self):
#        return War.objects.filter(
#            AdvancedQuery(war_clan_a=self.clan_a_id)
#        )

