from django.conf.urls import patterns, include, url
from django.contrib import admin

from coc_war_stats import views


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^clans/$', views.ClanIndex.as_view(), name='clans'),
    url(r'^clan/(?P<pk>\d+)/info/$', views.ClanInfo.as_view(), name='clan_info')
    #url(r'^clan/(?P<pk>\d+)/wars/$', views.ClanWarsIndex.as_view(), name='clan_wars')
)


